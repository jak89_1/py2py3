# Py2Py3
- Py2Py3 is a python(2) to python3 conversion tool
- version: `1.0.1-alpha` [ `WIP` / `testing` ]
- ci state: settup missing
- requirements: `python2.*` & `python3.*`
  - to get the latest updates you will also need `git`
# 

## Usage
- just execute the `py2py3` with `-h`, that should explain the most
- dont import it as module, it will fail anyways.

## FAQ
- why can't i execute it as root (sudo)?
  - this was made for safety reasons, we dont require root access, so we dont want to use it.

- i get a `invalid syntax` error, what am i doing wrong?
  - you need to have python3 installed to use the conversion tool (wouldnt make sense otherwise, right? ^^ )

## Help
- if you need help or found a bug, feel free to open an issue
- if you want to help, feel free to open a MR/PR :)

## Copyright & License
- [CC BY-SA 4.0](LICENSE)
